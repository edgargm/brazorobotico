//Estudiantes que desarrollaron este proyecto:
//- María Paula Peña Gamez (1802936)
//- Diana Andrea Espitia Torres (1802904)
//- Angel Jesús Estrada Anaya (1802905)
package dibujo3d; //paquete dibujo 3d

//librerias a utilizar, se importan a medida que las llamemos
import com.sun.j3d.loaders.IncorrectFormatException;
import com.sun.j3d.loaders.ParsingErrorException;
import com.sun.j3d.utils.behaviors.mouse.MouseBehavior;
import com.sun.j3d.utils.behaviors.mouse.MouseRotate;
import com.sun.j3d.utils.image.TextureLoader;
import com.sun.j3d.utils.universe.SimpleUniverse;
import gnu.io.SerialPortEvent;
import gnu.io.SerialPortEventListener;
import java.awt.GraphicsConfiguration;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.j3d.AmbientLight;
import javax.media.j3d.Background;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.ImageComponent2D;
import javax.media.j3d.IndexedQuadArray;
import javax.media.j3d.Shape3D;
import javax.media.j3d.Transform3D;
import javax.media.j3d.TransformGroup;
import javax.swing.Timer;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3d;
import javax.vecmath.Vector3f;
import org.jdesktop.j3d.loaders.vrml97.VrmlLoader;
import panamahitek.Arduino.PanamaHitek_Arduino;

public class BrazoRobotico extends javax.swing.JFrame implements Runnable { //Clase principal Brazo Robotico - Hereda de un JFrame - Implementa Runnable 

    //Declaracion de variables
    TextureLoader myLoader = new TextureLoader("fondo.jpg", this); // Aqui podemos encontrar la imagen que aparece como fondo del brazo (pared de ladrillos)
    ImageComponent2D myImage = myLoader.getImage();
    int dato = 0; //para indicar cuando gripper ha agarrago o no un objeto

    PanamaHitek_Arduino Arduino = new PanamaHitek_Arduino();  //Crear objeto Arduino de la Libreria PanamaHitek_Arduino para comunicación
    SerialPortEventListener evento = new SerialPortEventListener() {  //Se requiere instanciar la clase SerialPortEventListener, de la librería RXTX.

        @Override
        public void serialEvent(SerialPortEvent spe) {  //Permite el envio de mensajes a Java
            if (Arduino.isMessageAvailable() == true) { //Lee si hay mensaje enviado desde arduino
                String mensaje = (Arduino.printMessage()); //Para imprimir el mensaje de arduino de acuerdo a las condiciones
                if (mensaje.equals("Vacio")) { //Si el mensaje corresponde a "Vacio"
                    anuncio.setText("Gripper vacio"); //Se imprime en un TextField que el gripper está vacio
                    dato = 1; //Variable dato se iguala a 1
                }
                if (mensaje.equals("Ocupado")) { //Si el mensaje corresponde a "Ocupado"
                    anuncio.setText("Gripper ocupado"); //Se imprime en un TextField que el gripper está ocupado
                    dato = 0; //Variable dato se iguala a 0
                }
                if (mensaje.equals("Abrir")) { //Si el mensaje corresponde a "Abrir"
                    anuncio.setText("Abriendo Gripper"); //Se imprime en un TextField que el gripper está abriendo
                }
                if (mensaje.equals("Mas")) { //Si el mensaje corresponde a "Mas"
                    anuncio.setText("Girando muñeca mas"); //Se imprime en un TextField que esta girando muñeca mas
                }
                if (mensaje.equals("Menos")) { //Si el mensaje corresponde a "Menos"
                    anuncio.setText("Girando muñeca menos"); //Se imprime en un TextField que está girando muñeca menos
                }
                if (mensaje.equals("Derecha")) { //Si el mensaje corresponde a "Derecha"
                    anuncio.setText("Girando a la derecha"); //Se imprime en un TextField que el brazo está girando a la derecha
                }
                if (mensaje.equals("Izquierda")) { //Si el mensaje corresponde a "Izquierda"
                    anuncio.setText("Girando a la izquierda"); //Se imprime en un TextField que el brazo está girando a la izquierda
                }
                if (mensaje.equals("Bajar")) { //Si el mensaje corresponde a "Bajar"
                    anuncio.setText("Bajando brazo"); //Se imprime en un TextField que el brazo está bajando
                }
                if (mensaje.equals("Subir")) { //Si el mensaje corresponde a "Subir"
                    anuncio.setText("Subiendo brazo"); //Se imprime en un TextField que el brazo está subiendo
                }
            }
        }
    };

//De la clase BrazoRobotico
    public BrazoRobotico() {
        //Esto se realiza para que el compilar el programa, en la ventana donde se muestra la aplización el título sea Brazo Robótico
        init(); 
        setResizable(false);
        setSize(1200, 540);
        setTitle("Brazo Robótico");
        this.pack();
        setLocationRelativeTo(null);
    
    }
    private SimpleUniverse simpleU = null; //Crear el Universo 
    private TransformGroup obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8; //Creamos objetos
    Transform3D mover1 = new Transform3D(); //mover para abajo la figura 
    Transform3D mover2 = new Transform3D(); //mover pieza 3
    Transform3D rotar = new Transform3D();//mover pieza 3
    Transform3D transformar = new Transform3D();//mover pieza 3
    Transform3D mover3 = new Transform3D(); //mover pieza 4
    Transform3D rotar1 = new Transform3D();//mover pieza 4
    Transform3D transformar1 = new Transform3D();//mover pieza 4
    Transform3D mover4 = new Transform3D(); //mover pieza 5
    Transform3D mover5 = new Transform3D(); //mover pieza 6
    Transform3D transformar2 = new Transform3D();//mover pieza 6
    Transform3D mover6 = new Transform3D(); //mover pieza 8
    Transform3D rotar2 = new Transform3D(); //mover pinza 1
    Transform3D rotar3 = new Transform3D(); // mover pinza 2
    Transform3D transformar3 = new Transform3D(); //mover pieza 7
    Thread hilo = new Thread(this); //Crear hilo para las animaciones en Java
    //Restricciones del objeto
    int cont = 0; //Para movimiento abrir y cerrar
    int cont2 = 4; //Para movimiento girar+ - girar-
    int cont3 = 0; //Para movimiento girar derecha y girar izquierda
    String botonPresionado = ""; //Para identificar función a realizar según boton elegido por el usuario
    boolean movimientofinalizado = false; //Para que Java identifique cuando el movimiento ha finalizad
    int mov1 = 0, mov2 = 0; //Para movimientos de girar derecha/izquierda y subir/bajar brazo

    //Restricciones de la camara
    int rotx = 0;

    /**
     * Initializes the frame BrazoRobotico
     */
    public void init() {
  
        try {
            try {
                java.awt.EventQueue.invokeAndWait(new Runnable() {

                    public void run() {
                        
                        hilo.start();  //Inicio del hilo
                        initComponents(); //Permite cargar los objetos colocados en el Jframe al momento de ejercutar el programa
                        GraphicsConfiguration config = SimpleUniverse.getPreferredConfiguration(); //se crea objeto de la clase que describe las características de un destino de gráficos, en este caso la pantalla
                        Canvas3D canvas3D = new Canvas3D(config); //Se crea objeto de la clase Canvas3D, que proporciona un lienzo o plano de dibujo para el procesamiento en 3D

                        jPanel2.add("Center", canvas3D); //En un panel centrado

                        BranchGroup scene = createSceneGraph(); //Donde va todo lo que se crea - permite mover los elementos
                        scene.compile(); //Se agrega a la escena

                        simpleU = new SimpleUniverse(canvas3D);
                        simpleU.getViewingPlatform().setNominalViewingTransform();
                        simpleU.addBranchGraph(scene);
                        setDefaultCloseOperation(EXIT_ON_CLOSE);
                    }
                });
            } catch (Exception ex) {
                ex.printStackTrace();
            }

           // Arduino.arduinoRXTX("COM4", 9600, evento); //Comunicacion Java - Arduino para el enviar y recibir datos
        } catch (Exception ex) {
            Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void enviardatos(int numero) {  //Creamos metodos para enviar datos con sendByte a Arduino

        try {
            Arduino.sendByte(numero); //SendByte con el objeto de la clase PanamaHitek_Arduino
        } catch (Exception ex) {
            Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private BranchGroup createSceneGraph() {
        BranchGroup objRoot = new BranchGroup();

        //Inicializar objetos
        obj1 = new TransformGroup(); // base
        obj1.setCapability(TransformGroup.ALLOW_TRANSFORM_READ); //permite el acceso a la información de transformar su objeto.
        obj1.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);//permite escribir información de transformar su objeto.
        mover1.setTranslation(new Vector3f(0.0f, -0.9f, 0.0f)); //Para que las piezas coincidan con su respectivo lugar
        obj1.setTransform(mover1); //Tranformar el objeto de acuerdo a la posicion transladada

        obj2 = new TransformGroup(); // primer motor
        obj2.setCapability(TransformGroup.ALLOW_TRANSFORM_READ); //permite el acceso a la información de transformar su objeto.
        obj2.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE); //permite escribir información de transformar su objeto.

        obj3 = new TransformGroup(); //brazo 
        obj3.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);//permite el acceso a la información de transformar su objeto.
        obj3.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE); //permite escribir información de transformar su objeto.
        mover2.setTranslation(new Vector3f(0.0f, 0.055f, -0.04f));//Para que las piezas coincidan con su respectivo lugar
        transformar.mul(mover2); //multiplicar los transform 3D, implica que mover sera igual a transformar en su contenido 
        rotar.rotY(-Math.PI); //rotacion en el eje Y
        transformar.mul(rotar); //acumulando transformaciones
        obj3.setTransform(transformar); //transformar el objeto

        obj4 = new TransformGroup(); //motor paso a paso
        obj4.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);//permite el acceso a la información de transformar su objeto.
        obj4.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);//permite escribir información de transformar su objeto.
        mover3.setTranslation(new Vector3f(-0.135f, 0.04f, -0.041f)); //Para que las piezas coincidan con su respectivo lugar
        transformar1.mul(mover3); //multiplicar los transform 3D, implica que mover sera igual a transformar en su contenido 
        rotar1.rotY(-Math.PI / 2); //rotar objeto en el eje y
        transformar1.mul(rotar1); //acumulando transformaciones
        obj4.setTransform(transformar1); //transformar objeto

        obj5 = new TransformGroup(); // base giratoria motor
        obj5.setCapability(TransformGroup.ALLOW_TRANSFORM_READ); //permite el acceso a la información de transformar su objeto.
        obj5.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);//permite escribir información de transformar su objeto.
        mover4.setTranslation(new Vector3f(0.0f, 0.025f, 0.01f));//Para que las piezas coincidan con su respectivo lugar
        obj5.setTransform(mover4);//transformar objeto

        obj6 = new TransformGroup(); // gripper
        obj6.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);//permite el acceso a la información de transformar su objeto.
        obj6.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);//permite escribir información de transformar su objeto.
        mover5.setTranslation(new Vector3f(0.011f, 0.0f, 0.05f));//Para que las piezas coincidan con su respectivo lugar
        transformar2.mul(mover5); //multiplicar los transform 3D, implica que mover sera igual a transformar en su contenido 
        rotar.rotY(-Math.PI); //rotar objeto en el eje Y
        transformar2.mul(rotar); //acumulando transformaciones
        obj6.setTransform(transformar2); //transformar objeto

        obj7 = new TransformGroup(); // pinza 1 - derecha
        obj7.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);//permite el acceso a la información de transformar su objeto.
        obj7.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);//permite escribir información de transformar su objeto.
        rotar2.rotY(-Math.PI * 3 / 32); //rotar objeto en el eje Y
        obj7.setTransform(rotar2);

        obj8 = new TransformGroup(); // pinza 2 - izquierda
        obj8.setCapability(TransformGroup.ALLOW_TRANSFORM_READ);
        obj8.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        mover6.setTranslation(new Vector3f(0.025f, 0.0f, 0.0f));//Para que las piezas coincidan con su respectivo lugar
        transformar3.mul(mover6); //multiplicar los transform 3D, implica que mover sera igual a transformar en su contenido 
        rotar3.rotY(Math.PI * 3 / 32); //rotar objeto en el eje Y
        transformar3.mul(rotar3); //multiplicar los transform 3D, implica que rotar sera igual a transformar en su contenido 
        obj8.setTransform(transformar3);//transformar objeto

        //Insertar componenetes 
        obj1.addChild(loadGeometryWRL("motor1base.wrl")); // pieza hecha en SolidWorks
        objRoot.addChild(obj1);

        obj2.addChild(loadGeometryWRL("motor2base.wrl")); // pieza hecha en SolidWorks
        obj1.addChild(obj2);

        obj3.addChild(loadGeometryWRL("rotar.wrl")); // pieza hecha en SolidWorks
        obj2.addChild(obj3);

        obj4.addChild(loadGeometryWRL("motorp.wrl")); // pieza hecha en SolidWorks
        obj3.addChild(obj4);

        obj5.addChild(loadGeometryWRL("girar.wrl")); // pieza hecha en SolidWorks
        obj4.addChild(obj5);

        obj6.addChild(loadGeometryWRL("Gripper.wrl")); // pieza hecha en SolidWorks
        obj5.addChild(obj6);

        obj7.addChild(loadGeometryWRL("1pinza.wrl")); // pieza hecha en SolidWorks
        obj6.addChild(obj7);
        obj8.addChild(loadGeometryWRL("2pinza.wrl")); // pieza hecha en SolidWorks
        obj6.addChild(obj8);

        //luces
        objRoot.addChild(luces());

        //fondo
        objRoot.addChild(fondo());

        //piso
        objRoot.addChild(piso());

        return objRoot;

    }

    public BranchGroup loadGeometryWRL(String geometryURL) {
        BranchGroup objLoad = new BranchGroup();

        VrmlLoader wrl = new VrmlLoader();
        try {
            objLoad = wrl.load(geometryURL).getSceneGroup();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (ParsingErrorException ex) {
            ex.printStackTrace();
        } catch (IncorrectFormatException ex) {
            ex.printStackTrace();
        }
        return objLoad;
    }

    private TransformGroup fondo() { //fondo
        TransformGroup objRoot = new TransformGroup();
        Background font = new Background(myImage); //fondo
        font.setApplicationBounds(new BoundingSphere(new Point3d(), 100.0));
        objRoot.addChild(font);
        return objRoot;
    }

    private TransformGroup luces() { //luces
        TransformGroup objRoot = new TransformGroup();

        BoundingSphere bounds = new BoundingSphere(new Point3d(0, 0, 5), 100.0);
        Color3f lightColor = new Color3f(1.0f, 1.0f, 1.0f);
        Vector3f light1Direction = new Vector3f(0.0f, -1.0f, -1f);

        DirectionalLight luz1 = new DirectionalLight(lightColor, light1Direction);
        luz1.setInfluencingBounds(bounds);
        objRoot.addChild(luz1);

        AmbientLight luz2 = new AmbientLight(lightColor);
        luz2.setInfluencingBounds(bounds);
        objRoot.addChild(luz2);

        return objRoot;
    }

    private TransformGroup piso() { //piso
        TransformGroup sueloTransf = new TransformGroup();

        int tamano = 100;
        Point3f[] vertices = new Point3f[tamano * tamano];

        float inicio = -20.0f;
        float x = inicio;
        float z = inicio;

        float salto = 1.0f;

        int[] indices = new int[(tamano - 1) * (tamano - 1) * 4];
        int n = 0;

        //Colores establecidos para el piso
        Color3f blanco = new Color3f(1.0f, 1.0f, 1.0f);
        Color3f negro = new Color3f(1.0f, 1.0f, 1.0f);
        Color3f[] colors = {blanco, negro};

        int[] colorindices = new int[indices.length];

        for (int i = 0; i < tamano; i++) {
            for (int j = 0; j < tamano; j++) {
                vertices[i * tamano + j] = new Point3f(x, -1.0f, z);
                z += salto;
                if (i < (tamano - 1) && j < (tamano - 1)) {
                    int cindex = (i % 2 + j) % 2;
                    colorindices[n] = cindex;
                    indices[n++] = i * tamano + j;
                    colorindices[n] = cindex;
                    indices[n++] = i * tamano
                            + (j + 1);
                    colorindices[n] = cindex;
                    indices[n++] = (i + 1)
                            * tamano + (j + 1);
                    colorindices[n] = cindex;
                    indices[n++] = (i + 1)
                            * tamano + j;
                }
            }
            z = inicio;
            x += salto;
        }

        IndexedQuadArray geom = new IndexedQuadArray(vertices.length,
                GeometryArray.COORDINATES
                | GeometryArray.COLOR_3,
                indices.length);
        geom.setCoordinates(0, vertices);
        geom.setCoordinateIndices(0, indices);
        geom.setColors(0, colors);
        geom.setColorIndices(0, colorindices);

        Shape3D suelo = new Shape3D(geom);
        sueloTransf.addChild(suelo);

        return sueloTransf;
    }

    /**
     * This method is called from within the init() method to initialize the
     * form. WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        Inicial = new javax.swing.JButton();
        salir = new javax.swing.JButton();
        jPanel6 = new javax.swing.JPanel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        anuncio = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        AlejarCamara = new javax.swing.JButton();
        RotZmas = new javax.swing.JButton();
        RotZmenos = new javax.swing.JButton();
        RotXmas = new javax.swing.JButton();
        RotXmenos = new javax.swing.JButton();
        RotYmas = new javax.swing.JButton();
        RotYmenos = new javax.swing.JButton();
        BajarCamara = new javax.swing.JButton();
        CamaraXmenos = new javax.swing.JButton();
        AcercarCamara = new javax.swing.JButton();
        subirCamara = new javax.swing.JButton();
        CamaraXmas = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        Superior = new javax.swing.JButton();
        Frontal = new javax.swing.JButton();
        Posterior = new javax.swing.JButton();
        Isometrica = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        Cerrar = new javax.swing.JButton();
        Abrir = new javax.swing.JButton();
        Girarmuñemas = new javax.swing.JButton();
        GirarDbrazo = new javax.swing.JButton();
        Subirbrazo = new javax.swing.JButton();
        Girarmuñemenos = new javax.swing.JButton();
        GirarIbrazo = new javax.swing.JButton();
        Bajarbrazo = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setLayout(new java.awt.BorderLayout());
        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 10, 860, 480));

        Inicial.setBackground(new java.awt.Color(0, 0, 0));
        Inicial.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dibujo3d/camera.png"))); // NOI18N
        Inicial.setBorder(javax.swing.BorderFactory.createCompoundBorder(new javax.swing.border.MatteBorder(null), javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0))));
        Inicial.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        Inicial.setFocusable(false);
        Inicial.setHideActionText(true);
        Inicial.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/dibujo3d/camera.png"))); // NOI18N
        Inicial.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/dibujo3d/camera.png"))); // NOI18N
        Inicial.setRolloverSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/dibujo3d/camera.png"))); // NOI18N
        Inicial.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                InicialActionPerformed(evt);
            }
        });
        jPanel1.add(Inicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(108, 18, 100, 96));

        salir.setBackground(new java.awt.Color(204, 255, 255));
        salir.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        salir.setText("Salir");
        salir.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirActionPerformed(evt);
            }
        });
        jPanel1.add(salir, new org.netbeans.lib.awtextra.AbsoluteConstraints(948, 624, 90, 40));

        jPanel6.setBackground(new java.awt.Color(0, 0, 0));

        jComboBox1.setBackground(new java.awt.Color(204, 255, 255));
        jComboBox1.setFont(new java.awt.Font("PMingLiU-ExtB", 0, 11)); // NOI18N
        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] {"", " Ruta 1", " Ruta 2", " Reiniciar"}));
        jComboBox1.setName("Rutas"); // NOI18N
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Century Schoolbook", 3, 18)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Rutas ");

        jLabel9.setFont(new java.awt.Font("Century Schoolbook", 3, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 255, 255));
        jLabel9.setText("Estado del Gripper");

        anuncio.setFont(new java.awt.Font("PMingLiU-ExtB", 0, 11)); // NOI18N

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(35, 35, 35)
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                        .addComponent(anuncio, javax.swing.GroupLayout.PREFERRED_SIZE, 174, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(77, 77, 77)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel9)))
                .addGap(22, 22, 22))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(anuncio, javax.swing.GroupLayout.DEFAULT_SIZE, 29, Short.MAX_VALUE))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 528, 410, 70));

        jPanel3.setBackground(new java.awt.Color(0, 0, 0));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Century Schoolbook", 3, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText(" Movimiento de Cámara ");
        jPanel3.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 10, 230, -1));

        AlejarCamara.setBackground(new java.awt.Color(255, 255, 255));
        AlejarCamara.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        AlejarCamara.setText("-");
        AlejarCamara.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        AlejarCamara.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AlejarCamaraActionPerformed(evt);
            }
        });
        jPanel3.add(AlejarCamara, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 60, -1));

        RotZmas.setBackground(new java.awt.Color(255, 255, 255));
        RotZmas.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        RotZmas.setText("Rot Z+");
        RotZmas.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        RotZmas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RotZmasActionPerformed(evt);
            }
        });
        jPanel3.add(RotZmas, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, 90, -1));

        RotZmenos.setBackground(new java.awt.Color(255, 255, 255));
        RotZmenos.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        RotZmenos.setText("Rot Z-");
        RotZmenos.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        RotZmenos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RotZmenosActionPerformed(evt);
            }
        });
        jPanel3.add(RotZmenos, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 170, 90, -1));

        RotXmas.setBackground(new java.awt.Color(255, 255, 255));
        RotXmas.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        RotXmas.setText("Rot X+");
        RotXmas.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        RotXmas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RotXmasActionPerformed(evt);
            }
        });
        jPanel3.add(RotXmas, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 90, -1));

        RotXmenos.setBackground(new java.awt.Color(255, 255, 255));
        RotXmenos.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        RotXmenos.setText("Rot X-");
        RotXmenos.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        RotXmenos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RotXmenosActionPerformed(evt);
            }
        });
        jPanel3.add(RotXmenos, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 140, 90, -1));

        RotYmas.setBackground(new java.awt.Color(255, 255, 255));
        RotYmas.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        RotYmas.setText("Rot Y+");
        RotYmas.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        RotYmas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RotYmasActionPerformed(evt);
            }
        });
        jPanel3.add(RotYmas, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 90, -1));

        RotYmenos.setBackground(new java.awt.Color(255, 255, 255));
        RotYmenos.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        RotYmenos.setText("Rot Y-");
        RotYmenos.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        RotYmenos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RotYmenosActionPerformed(evt);
            }
        });
        jPanel3.add(RotYmenos, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 110, 90, -1));

        BajarCamara.setBackground(new java.awt.Color(255, 255, 255));
        BajarCamara.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        BajarCamara.setText("Y-");
        BajarCamara.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        BajarCamara.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BajarCamaraActionPerformed(evt);
            }
        });
        jPanel3.add(BajarCamara, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 80, 60, -1));

        CamaraXmenos.setBackground(new java.awt.Color(255, 255, 255));
        CamaraXmenos.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        CamaraXmenos.setText("X-");
        CamaraXmenos.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        CamaraXmenos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CamaraXmenosActionPerformed(evt);
            }
        });
        jPanel3.add(CamaraXmenos, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 80, 60, -1));

        AcercarCamara.setBackground(new java.awt.Color(255, 255, 255));
        AcercarCamara.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        AcercarCamara.setText("+");
        AcercarCamara.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        AcercarCamara.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AcercarCamaraActionPerformed(evt);
            }
        });
        jPanel3.add(AcercarCamara, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 50, 60, -1));

        subirCamara.setBackground(new java.awt.Color(255, 255, 255));
        subirCamara.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        subirCamara.setText("Y+");
        subirCamara.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        subirCamara.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                subirCamaraActionPerformed(evt);
            }
        });
        jPanel3.add(subirCamara, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 50, 60, -1));

        CamaraXmas.setBackground(new java.awt.Color(255, 255, 255));
        CamaraXmas.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        CamaraXmas.setText("X+");
        CamaraXmas.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        CamaraXmas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CamaraXmasActionPerformed(evt);
            }
        });
        jPanel3.add(CamaraXmas, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 50, 60, -1));

        jPanel1.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(48, 120, 228, 210));

        jPanel4.setBackground(new java.awt.Color(0, 0, 0));

        jLabel2.setFont(new java.awt.Font("Century Schoolbook", 3, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Vistas ");

        Superior.setBackground(new java.awt.Color(255, 255, 255));
        Superior.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        Superior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dibujo3d/image/superior.png"))); // NOI18N
        Superior.setText("Superior");
        Superior.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Superior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SuperiorActionPerformed(evt);
            }
        });

        Frontal.setBackground(new java.awt.Color(255, 255, 255));
        Frontal.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        Frontal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dibujo3d/image/frontal.png"))); // NOI18N
        Frontal.setText("Frontal");
        Frontal.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Frontal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                FrontalActionPerformed(evt);
            }
        });

        Posterior.setBackground(new java.awt.Color(255, 255, 255));
        Posterior.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        Posterior.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dibujo3d/image/posterior.png"))); // NOI18N
        Posterior.setText("Posterior");
        Posterior.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Posterior.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PosteriorActionPerformed(evt);
            }
        });

        Isometrica.setBackground(new java.awt.Color(255, 255, 255));
        Isometrica.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        Isometrica.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dibujo3d/image/isometrico.png"))); // NOI18N
        Isometrica.setText("Isometrica");
        Isometrica.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Isometrica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IsometricaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(77, 77, 77))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(Superior, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(Frontal, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(Posterior, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                        .addComponent(Isometrica, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Superior)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Frontal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Posterior)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(Isometrica, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(18, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(48, 360, 220, 240));

        jPanel5.setBackground(new java.awt.Color(0, 0, 0));

        jLabel4.setFont(new java.awt.Font("Century Schoolbook", 3, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Movimientos del Brazo ");

        Cerrar.setBackground(new java.awt.Color(255, 255, 255));
        Cerrar.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        Cerrar.setText("Cerrar");
        Cerrar.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Cerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CerrarActionPerformed(evt);
            }
        });

        Abrir.setBackground(new java.awt.Color(255, 255, 255));
        Abrir.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        Abrir.setText("Abrir");
        Abrir.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Abrir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AbrirActionPerformed(evt);
            }
        });

        Girarmuñemas.setBackground(new java.awt.Color(255, 255, 255));
        Girarmuñemas.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        Girarmuñemas.setText("Girar muñeca +");
        Girarmuñemas.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Girarmuñemas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GirarmuñemasActionPerformed(evt);
            }
        });

        GirarDbrazo.setBackground(new java.awt.Color(255, 255, 255));
        GirarDbrazo.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        GirarDbrazo.setText("Girar a la derecha");
        GirarDbrazo.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        GirarDbrazo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GirarDbrazoActionPerformed(evt);
            }
        });

        Subirbrazo.setBackground(new java.awt.Color(255, 255, 255));
        Subirbrazo.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        Subirbrazo.setText("Subir brazo");
        Subirbrazo.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Subirbrazo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubirbrazoActionPerformed(evt);
            }
        });

        Girarmuñemenos.setBackground(new java.awt.Color(255, 255, 255));
        Girarmuñemenos.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        Girarmuñemenos.setText("Girar muñeca -");
        Girarmuñemenos.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Girarmuñemenos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GirarmuñemenosActionPerformed(evt);
            }
        });

        GirarIbrazo.setBackground(new java.awt.Color(255, 255, 255));
        GirarIbrazo.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        GirarIbrazo.setText("Girar a la izquierda");
        GirarIbrazo.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        GirarIbrazo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                GirarIbrazoActionPerformed(evt);
            }
        });

        Bajarbrazo.setBackground(new java.awt.Color(255, 255, 255));
        Bajarbrazo.setFont(new java.awt.Font("PMingLiU-ExtB", 3, 14)); // NOI18N
        Bajarbrazo.setText("Bajar brazo");
        Bajarbrazo.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        Bajarbrazo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BajarbrazoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(116, 116, 116)
                .addComponent(jLabel4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Abrir, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(GirarIbrazo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Bajarbrazo, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Girarmuñemenos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Subirbrazo, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(GirarDbrazo, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Girarmuñemas, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Cerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Cerrar)
                    .addComponent(Abrir))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(Girarmuñemas)
                    .addComponent(Girarmuñemenos, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(GirarDbrazo)
                    .addComponent(GirarIbrazo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Subirbrazo)
                    .addComponent(Bajarbrazo))
                .addContainerGap(18, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(324, 528, 440, 180));

        jLabel5.setFont(new java.awt.Font("Yu Gothic UI Light", 3, 24)); // NOI18N
        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/dibujo3d/espacio.jpg"))); // NOI18N
        jLabel5.setText("jLabel5");
        jLabel5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1210, 744));

        jLabel6.setText("jLabel6");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 550, -1, -1));

        jLabel7.setText("jLabel7");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 550, -1, -1));

        jLabel8.setFont(new java.awt.Font("Yu Gothic UI Light", 3, 24)); // NOI18N
        jLabel8.setText("Movimientos del Brazo ");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(1025, 690, 160, -1));

        jLabel10.setText("jLabel10");
        jPanel1.add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(1100, 650, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
    }// </editor-fold>//GEN-END:initComponents

private void CamaraXmenosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CamaraXmenosActionPerformed
//Movimiento de camara    
    //Obtener TransformGroup camara
    TransformGroup universo
            = simpleU.getViewingPlatform().getViewPlatformTransform();

    //Obtener posicion de la camara con un transform3d
    Transform3D actual = new Transform3D();
    universo.getTransform(actual);

    //Crear un incremento
    Transform3D inc = new Transform3D();
    inc.set(new Vector3d(-0.1, 0, 0));
    //Multiplicar posicion actual por incremento
    actual.mul(inc);
    //Escribir resultado de la nueva posicion
    universo.setTransform(actual);
}//GEN-LAST:event_CamaraXmenosActionPerformed

private void CamaraXmasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CamaraXmasActionPerformed
//Movimiento de camara       
    //Obtener TransformGroup camara
    TransformGroup universo
            = simpleU.getViewingPlatform().getViewPlatformTransform();

    //Obtener posicion de la camara con un transform3d
    Transform3D actual = new Transform3D();
    universo.getTransform(actual);

    //Crear un incremento
    Transform3D inc = new Transform3D();
    inc.set(new Vector3d(0.1, 0, 0));
    //Multiplicar posicion actual por incremento
    actual.mul(inc);
    //Escribir resultado de la nueva posicion
    universo.setTransform(actual);
}//GEN-LAST:event_CamaraXmasActionPerformed

private void RotYmasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RotYmasActionPerformed
//Movimiento de camara rotar 
    //Obtener TransformGroup camara
    TransformGroup universo
            = simpleU.getViewingPlatform().getViewPlatformTransform();

    //Obtener posicion de la camara con un transform3d
    Transform3D actual = new Transform3D();
    universo.getTransform(actual);

    //Crear un incremento
    Transform3D inc = new Transform3D();
    inc.rotY(-Math.PI / 32);
    //Multiplicar posicion actual por incremento
    actual.mul(inc);
    //Escribir resultado de la nueva posicion
    universo.setTransform(actual);
}//GEN-LAST:event_RotYmasActionPerformed

private void BajarCamaraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BajarCamaraActionPerformed
//Movimiento de camara 
    //Obtener TransformGroup camara
    TransformGroup universo
            = simpleU.getViewingPlatform().getViewPlatformTransform();

    //Obtener posicion de la camara con un transform3d
    Transform3D actual = new Transform3D();
    universo.getTransform(actual);

    //Crear un incremento
    Transform3D inc = new Transform3D();
    inc.set(new Vector3d(0, -0.1, 0));
    //Multiplicar posicion actual por incremento
    actual.mul(inc);
    //Escribir resultado de la nueva posicion
    universo.setTransform(actual);
}//GEN-LAST:event_BajarCamaraActionPerformed

private void AlejarCamaraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AlejarCamaraActionPerformed
//Movimiento de camara 
    //Obtener TransformGroup camara
    TransformGroup universo
            = simpleU.getViewingPlatform().getViewPlatformTransform();

    //Obtener posicion de la camara con un transform3d
    Transform3D actual = new Transform3D();
    universo.getTransform(actual);

    //Crear un incremento
    Transform3D inc = new Transform3D();
    inc.set(new Vector3d(0, 0, 0.1));
    //Multiplicar posicion actual por incremento
    actual.mul(inc);
    //Escribir resultado de la nueva posicion
    universo.setTransform(actual);
}//GEN-LAST:event_AlejarCamaraActionPerformed

private void AcercarCamaraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AcercarCamaraActionPerformed
//Movimiento de camara 
    //Obtener TransformGroup camara
    TransformGroup universo
            = simpleU.getViewingPlatform().getViewPlatformTransform();

    //Obtener posicion de la camara con un transform3d
    Transform3D actual = new Transform3D();
    universo.getTransform(actual);

    //Crear un incremento
    Transform3D inc = new Transform3D();
    inc.set(new Vector3d(0, 0, -0.1));
    //Multiplicar posicion actual por incremento
    actual.mul(inc);
    //Escribir resultado de la nueva posicion
    universo.setTransform(actual);
}//GEN-LAST:event_AcercarCamaraActionPerformed

    private void subirCamaraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_subirCamaraActionPerformed
        //Movimiento de camara 
        //Obtener TransformGroup camara
        TransformGroup universo
                = simpleU.getViewingPlatform().getViewPlatformTransform();

        //Obtener posicion de la camara con un transform3d
        Transform3D actual = new Transform3D();
        universo.getTransform(actual);

        //Crear un incremento
        Transform3D inc = new Transform3D();
        inc.set(new Vector3d(0, 0.1, 0));
        //Multiplicar posicion actual por incremento
        actual.mul(inc);
        //Escribir resultado de la nueva posicion
        universo.setTransform(actual);        // TODO add your handling code here:
    }//GEN-LAST:event_subirCamaraActionPerformed

    private void CerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CerrarActionPerformed
        //Cerrar Gripper
        botonPresionado = "cerrar"; //para saber a que corresponde el botón presionado
        movimientofinalizado = true; //Boolean movimiento finalizado
    }//GEN-LAST:event_CerrarActionPerformed

    private void AbrirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AbrirActionPerformed
        //Abrir Gripper
        botonPresionado = "abrir"; //para saber a que corresponde el botón presionado
        movimientofinalizado = true; //Boolean movimiento finalizado
    }//GEN-LAST:event_AbrirActionPerformed

    private void GirarmuñemenosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GirarmuñemenosActionPerformed
        //Girar Muñeca
        botonPresionado = "giramuñeca-"; //para saber a que corresponde el botón presionado
        movimientofinalizado = true; //Boolean movimiento finalizado
//        
    }//GEN-LAST:event_GirarmuñemenosActionPerformed

    private void GirarmuñemasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GirarmuñemasActionPerformed
        //Girar Muñeca
        botonPresionado = "giramuñeca+"; //para saber a que corresponde el botón presionado
        movimientofinalizado = true; //Boolean movimiento finalizado

    }//GEN-LAST:event_GirarmuñemasActionPerformed

    private void BajarbrazoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BajarbrazoActionPerformed
        //bajar brazo
        botonPresionado = "bajar"; //para saber a que corresponde el botón presionado
        movimientofinalizado = true;//Boolean movimiento finalizado
    }//GEN-LAST:event_BajarbrazoActionPerformed

    private void SubirbrazoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubirbrazoActionPerformed
        //Subir brazo
        botonPresionado = "subir"; //para saber a que corresponde el botón presionado
        movimientofinalizado = true; //Boolean movimiento finalizado
    }//GEN-LAST:event_SubirbrazoActionPerformed

    private void GirarIbrazoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GirarIbrazoActionPerformed
        //movimiento de brazo
        botonPresionado = "girarizquierda"; //para saber a que corresponde el botón presionado
        movimientofinalizado = true; //Boolean movimiento finalizado
    }//GEN-LAST:event_GirarIbrazoActionPerformed

    private void GirarDbrazoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_GirarDbrazoActionPerformed
        //movimiento de brazo
        botonPresionado = "girarderecha"; //para saber a que corresponde el botón presionado
        movimientofinalizado = true; //Boolean movimiento finalizado

    }//GEN-LAST:event_GirarDbrazoActionPerformed

    private void RotYmenosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RotYmenosActionPerformed
        //Movimiento de camara rotar 
        //Obtener TransformGroup camara
        TransformGroup universo
                = simpleU.getViewingPlatform().getViewPlatformTransform();

        //Obtener posicion de la camara con un transform3d
        Transform3D actual = new Transform3D();
        universo.getTransform(actual);

        //Crear un incremento
        Transform3D inc = new Transform3D();
        inc.rotY(Math.PI / 32);
        //Multiplicar posicion actual por incremento
        actual.mul(inc);
        //Escribir resultado de la nueva posicion
        universo.setTransform(actual);
    }//GEN-LAST:event_RotYmenosActionPerformed

    private void RotXmasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RotXmasActionPerformed
//Movimiento de camara rotar 
        rotx++;
//Obtener TransformGroup camara
        TransformGroup universo
                = simpleU.getViewingPlatform().getViewPlatformTransform();

        //Obtener posicion de la camara con un transform3d
        Transform3D actual = new Transform3D();
        universo.getTransform(actual);

        //Crear un incremento
        Transform3D inc = new Transform3D();
        inc.rotX(-Math.PI / 32);
        //Multiplicar posicion actual por incremento
        actual.mul(inc);
        //Escribir resultado de la nueva posicion
        universo.setTransform(actual);
    }//GEN-LAST:event_RotXmasActionPerformed

    private void RotXmenosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RotXmenosActionPerformed
        //Movimiento de camara rotar  
        rotx--;
        //Obtener TransformGroup camara
        TransformGroup universo
                = simpleU.getViewingPlatform().getViewPlatformTransform();

        //Obtener posicion de la camara con un transform3d
        Transform3D actual = new Transform3D();
        universo.getTransform(actual);

        //Crear un incremento
        Transform3D inc = new Transform3D();
        inc.rotX(Math.PI / 32);
        //Multiplicar posicion actual por incremento
        actual.mul(inc);
        //Escribir resultado de la nueva posicion
        universo.setTransform(actual);
    }//GEN-LAST:event_RotXmenosActionPerformed

    private void RotZmasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RotZmasActionPerformed
//Movimiento de camara rotar 
        //Obtener TransformGroup camara
        TransformGroup universo
                = simpleU.getViewingPlatform().getViewPlatformTransform();

        //Obtener posicion de la camara con un transform3d
        Transform3D actual = new Transform3D();
        universo.getTransform(actual);

        //Crear un incremento
        Transform3D inc = new Transform3D();
        inc.rotZ(-Math.PI / 32);
        //Multiplicar posicion actual por incremento
        actual.mul(inc);
        //Escribir resultado de la nueva posicion
        universo.setTransform(actual);
    }//GEN-LAST:event_RotZmasActionPerformed

    private void RotZmenosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RotZmenosActionPerformed
//Movimiento de camara rotar 
        //Obtener TransformGroup camara
        TransformGroup universo
                = simpleU.getViewingPlatform().getViewPlatformTransform();

        //Obtener posicion de la camara con un transform3d
        Transform3D actual = new Transform3D();
        universo.getTransform(actual);

        //Crear un incremento
        Transform3D inc = new Transform3D();
        inc.rotZ(Math.PI / 32);
        //Multiplicar posicion actual por incremento
        actual.mul(inc);
        //Escribir resultado de la nueva posicion
        universo.setTransform(actual);
    }//GEN-LAST:event_RotZmenosActionPerformed

    private void SuperiorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SuperiorActionPerformed
//Vistas    
//Obtener TransformGroup camara
        TransformGroup universo
                = simpleU.getViewingPlatform().getViewPlatformTransform();

        //Obtener posicion de la camara con un transform3d
        Transform3D actual = new Transform3D();

        //Crear un incremento
        Transform3D inc = new Transform3D();
        inc.set(new Vector3d(0, 0, 0));
        //Multiplicar posicion actual por incremento
        actual.mul(inc);
        inc.rotX(-Math.PI / 2);
        actual.mul(inc);
        //Escribir resultado de la nueva posicion
        universo.setTransform(actual);
    }//GEN-LAST:event_SuperiorActionPerformed

    private void InicialActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_InicialActionPerformed
//Vista Inicial    
//Obtener TransformGroup camara
        TransformGroup universo
                = simpleU.getViewingPlatform().getViewPlatformTransform();

        //Obtener posicion de la camara con un transform3d
        Transform3D actual = new Transform3D();

        //Crear un incremento
        Transform3D inc = new Transform3D();
        inc.set(new Vector3d(0, 1, 0));
        //Multiplicar posicion actual por incremento
        actual.mul(inc);
        inc.rotX(-Math.PI / 2);
        actual.mul(inc);

        inc.rotX(Math.PI / 2);
        actual.mul(inc);
        Transform3D inc2 = new Transform3D();
        inc2.set(new Vector3d(0, -1.8, 1));
        //Multiplicar posicion actual por incremento
        actual.mul(inc2);

        //Escribir resultado de la nueva posicion
        universo.setTransform(actual);
    }//GEN-LAST:event_InicialActionPerformed

    private void FrontalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_FrontalActionPerformed
//Vistas    
//Obtener TransformGroup camara
        TransformGroup universo
                = simpleU.getViewingPlatform().getViewPlatformTransform();

        //Obtener posicion de la camara con un transform3d
        Transform3D actual = new Transform3D();

        //Crear un incremento
        Transform3D inc = new Transform3D();
        inc.set(new Vector3d(1, -0.8, 0));
        //Multiplicar posicion actual por incremento
        actual.mul(inc);
        inc.rotY(Math.PI / 2);
        actual.mul(inc);
        //Escribir resultado de la nueva posicion
        universo.setTransform(actual);

    }//GEN-LAST:event_FrontalActionPerformed

    private void PosteriorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PosteriorActionPerformed
//Vistas    
//Obtener TransformGroup camara
        TransformGroup universo
                = simpleU.getViewingPlatform().getViewPlatformTransform();

        //Obtener posicion de la camara con un transform3d
        Transform3D actual = new Transform3D();

        //Crear un incremento
        Transform3D inc = new Transform3D();
        inc.set(new Vector3d(-1, -0.7, 0));
        //Multiplicar posicion actual por incremento
        actual.mul(inc);
        inc.rotY(-Math.PI / 2);
        actual.mul(inc);
        //Escribir resultado de la nueva posicion
        universo.setTransform(actual);
    }//GEN-LAST:event_PosteriorActionPerformed

    private void IsometricaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IsometricaActionPerformed
//Vistas    
//Obtener TransformGroup camara
        TransformGroup universo
                = simpleU.getViewingPlatform().getViewPlatformTransform();

        //Obtener posicion de la camara con un transform3d
        Transform3D actual = new Transform3D();

        //Crear un incremento
        Transform3D inc = new Transform3D();
        inc.set(new Vector3d(0, 1, 0));
        //Multiplicar posicion actual por incremento
        actual.mul(inc);
        inc.rotX(-Math.PI / 2);
        actual.mul(inc);

        inc.rotX(Math.PI / 2);
        actual.mul(inc);
        Transform3D inc2 = new Transform3D();
        inc2.set(new Vector3d(0, -1.8, 1));
        //Multiplicar posicion actual por incremento
        actual.mul(inc2);

        Transform3D inc3 = new Transform3D();
        inc3.set(new Vector3d(0, 0.1, -0.1));
        actual.mul(inc3);
        inc3.rotY(Math.PI / 8);
        actual.mul(inc3);
        inc3.rotX(-Math.PI / 16);
        actual.mul(inc3);

        Transform3D inc4 = new Transform3D();
        inc4.set(new Vector3d(0.4, 0.1, 0));
        actual.mul(inc4);
        inc4.rotX(-Math.PI / 32);
        actual.mul(inc4);
        //Escribir resultado de la nueva posicion
        universo.setTransform(actual);
    }//GEN-LAST:event_IsometricaActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        //Se crea un menú para seleccionar las rutas
        if (jComboBox1.getSelectedIndex() == 1) { //Item seleccionado 1
            botonPresionado = "ruta1"; //para saber a que corresponde el botón presionado
            movimientofinalizado = true; //Boolean para movimiento finalizado
        }
        if (jComboBox1.getSelectedIndex() == 2) { //Item seleccionado 2 
            botonPresionado = "ruta2";//para saber a que corresponde el botón presionado
            movimientofinalizado = true; //Boolean para movimiento finalizado
        }
        if (jComboBox1.getSelectedIndex() == 3) { //Item seleccionado 3
            botonPresionado = "reiniciar";//para saber a que corresponde el botón presionado
            movimientofinalizado = true; //Boolean para movimiento finalizado
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirActionPerformed
        System.exit(0); //Opción para salir del programa - aplicación
    }//GEN-LAST:event_salirActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Abrir;
    private javax.swing.JButton AcercarCamara;
    private javax.swing.JButton AlejarCamara;
    private javax.swing.JButton BajarCamara;
    private javax.swing.JButton Bajarbrazo;
    private javax.swing.JButton CamaraXmas;
    private javax.swing.JButton CamaraXmenos;
    private javax.swing.JButton Cerrar;
    private javax.swing.JButton Frontal;
    private javax.swing.JButton GirarDbrazo;
    private javax.swing.JButton GirarIbrazo;
    private javax.swing.JButton Girarmuñemas;
    private javax.swing.JButton Girarmuñemenos;
    private javax.swing.JButton Inicial;
    private javax.swing.JButton Isometrica;
    private javax.swing.JButton Posterior;
    private javax.swing.JButton RotXmas;
    private javax.swing.JButton RotXmenos;
    private javax.swing.JButton RotYmas;
    private javax.swing.JButton RotYmenos;
    private javax.swing.JButton RotZmas;
    private javax.swing.JButton RotZmenos;
    private javax.swing.JButton Subirbrazo;
    private javax.swing.JButton Superior;
    private javax.swing.JTextField anuncio;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JButton salir;
    private javax.swing.JButton subirCamara;
    // End of variables declaration//GEN-END:variables

    //Creacion de Metodos
    //Girar muñeca menos
    public void giramuñecamenos() {
        Transform3D actual = new Transform3D();
        obj5.getTransform(actual);
        Transform3D inc = new Transform3D();

        inc.rotZ(-Math.PI / 170); //Cuanto debe girar
        actual.mul(inc);
        obj5.setTransform(actual);

        try {
            Thread.sleep(25); //Hilo duerme
        } catch (InterruptedException ex) {
            Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Girara muñeca mas
    public void giramuñecamas() {
        Transform3D actual = new Transform3D();
        obj5.getTransform(actual);
        Transform3D inc = new Transform3D();

        inc.rotZ(Math.PI / 170); //Cuanto debe girar
        actual.mul(inc);
        obj5.setTransform(actual);
        try {
            Thread.sleep(25); //Hilo duerme
        } catch (InterruptedException ex) {
            Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Abrir Gripper
    public void abrirGripper() {
        Transform3D actual = new Transform3D();
        obj7.getTransform(actual);
        Transform3D inc = new Transform3D();

        inc.rotY(Math.PI / 1200); //Cuanto debe girar
        actual.mul(inc);
        obj7.setTransform(actual);

        Transform3D actual1 = new Transform3D();
        obj8.getTransform(actual1);
        Transform3D inc1 = new Transform3D();

        inc1.rotY(-Math.PI / 1200); //Cuanto debe girar
        actual1.mul(inc1);
        obj8.setTransform(actual1);
        try {
            Thread.sleep(28); //Hilo duerme
        } catch (InterruptedException ex) {
            Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Cerrar Gripper
    public void cerrarGripper() {
        Transform3D actual = new Transform3D();
        obj7.getTransform(actual);
        Transform3D inc = new Transform3D();

        inc.rotY(-Math.PI / 1200); //Cuanto debe girar
        actual.mul(inc);
        obj7.setTransform(actual);

        Transform3D actual1 = new Transform3D();
        obj8.getTransform(actual1);
        Transform3D inc1 = new Transform3D();

        inc1.rotY(Math.PI / 1200); //Cuanto debe girar
        actual1.mul(inc1);
        obj8.setTransform(actual1);

        try {
            Thread.sleep(18); //Hilo duerme
        } catch (InterruptedException ex) {
            Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    //Girar derecha
    public void GirarDerecha() {
        Transform3D actual = new Transform3D();
        obj2.getTransform(actual);
        Transform3D inc = new Transform3D();

        inc.rotY(Math.PI / 100); //Cuanto debe girar
        actual.mul(inc);
        obj2.setTransform(actual);

        try {
            Thread.sleep(45); //Hilo duerme
        } catch (InterruptedException ex) {
            Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Girar izquierda
    public void GirarIzquierda() {
        Transform3D actual = new Transform3D();
        obj2.getTransform(actual);
        Transform3D inc = new Transform3D();

        inc.rotY(-Math.PI / 100); //Cuanto debe girar
        actual.mul(inc);
        obj2.setTransform(actual);

        try {
            Thread.sleep(45); //Hilo duerme
        } catch (InterruptedException ex) {
            Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Subir Brazo
    public void SubirBrazo() {
        Transform3D actual = new Transform3D();
        obj3.getTransform(actual);
        Transform3D inc = new Transform3D();

        inc.rotZ(-Math.PI / 100); //Cuanto debe girar
        actual.mul(inc);
        obj3.setTransform(actual);

        try {
            Thread.sleep(40); //Hilo duerme 40 ms
        } catch (InterruptedException ex) {
            Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Bajar brazo
    public void BajarBrazo() {
        Transform3D actual = new Transform3D();
        obj3.getTransform(actual);
        Transform3D inc = new Transform3D();

        inc.rotZ(Math.PI / 100); //Cuanto debe girar
        actual.mul(inc);
        obj3.setTransform(actual);

        try {
            Thread.sleep(40); //Hilo duerme 40 ms
        } catch (InterruptedException ex) {
            Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Funcionamiento del Hilo 
    public void run() { //para esto implementamos Runnable

        while (true) {

            if (movimientofinalizado == true) { //Se declaró en cada uno de los botones

                if (botonPresionado.equals("giramuñeca+")) { //Botón girar muñeca mas

                    if (cont2 < 8 && cont2 >= 0) { //Restriccion para girar muñeca +
                        enviardatos(3); //Enviar dato 3 a Arduino para efectuar la respectiva accion - girar muñeca +
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            giramuñecamas(); //Llamar al método
                        }
                        cont2++; //Contador restricion
                    }

                    movimientofinalizado = false; //Indicar que el movimiento ha finalizado y se debe detener en java
                }
                if (botonPresionado.equals("giramuñeca-")) { //Botón girar muñeca menos

                    if (cont2 > 0 && cont2 <= 8) { //Restriccion girar muñeca -
                        enviardatos(4); //Enviar dato 4 a Arduino para efectuar la respectiva accion - girar muñeca -
                        for (int k = 0; k <= 10; k++) {//Animacion en java se repite 10 veces
                            giramuñecamenos(); //Llamar al método

                        }
                        cont2--; //Contador restriccion
                    }

                    movimientofinalizado = false;//Indicar que el movimiento ha finalizado y se debe detener en java

                }

                if (botonPresionado.equals("abrir")) { //Botón abrir Gripper
                    if (cont < 10) { //Restriccion abrir gripper
                        enviardatos(1); //Enviar dato 1 a Arduino para efectuar la respectiva accion - abrir gripper
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            abrirGripper(); //Llamar al método
                        }
                        cont++; //Contador restricción
                    }
                    movimientofinalizado = false; //Indicar que el movimiento ha finalizado y se debe detener en java
                }

                if (botonPresionado.equals("cerrar")) { //Botón cerrar Gripper

                    if (cont > 0) { //Restriccion cerrar gripper
                        enviardatos(2); //Enviar dato 1 a Arduino para efectuar la respectiva accion - abrir gripper
                        if (dato == 1) {
                            for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                                cerrarGripper(); //Llamar al método
                            }
                            cont--; //Contador restricción
                        }

                    }
                    movimientofinalizado = false; //Indicar que el movimiento ha finalizado y se debe detener en java
                }

                if (botonPresionado.equals("girarderecha")) { //Botón girar derecha
                    if (mov1 > 0) { //Restriccion girar brazo a la derecha
                        enviardatos(6); //Enviar dato 6 a Arduino para efectuar la respectiva accion - girar brazo a la derecha
                        for (int k = 0; k <= 10; k++) {//Animacion en java se repite 10 veces
                            GirarDerecha(); //Llamar al método
                        }
                        mov1--; //Contador restriccion 
                    }
                    movimientofinalizado = false; //Indicar que el movimiento ha finalizado y se debe detener en java
                }

                if (botonPresionado.equals("girarizquierda")) { //Botón girar izquierda
                    if (mov1 < 1) { //Restriccion girar brazo a la izquierda
                        enviardatos(5); //Enviar dato 5 a Arduino para efectuar la respectiva accion - girar brazo a la izquierda
                        for (int k = 0; k <= 10; k++) {//Animacion en java se repite 10 veces
                            GirarIzquierda(); //Llamar al método
                        }
                        mov1++; //Contador restriccion
                    }
                    movimientofinalizado = false; //Indicar que el movimiento ha finalizado y se debe detener en java
                }

                if (botonPresionado.equals("subir")) { //Botón subir brazo
                    if (mov2 > 0) { //Restricción subir brazo
                        enviardatos(8); //Enviar dato 8 a Arduino para efectuar la respectiva accion - subir brazo
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            SubirBrazo(); //Llamar al método
                        }
                        mov2--; //Contador restricción
                    }
                    movimientofinalizado = false; //Indicar que el movimiento ha finalizado y se debe detener en java
                }

                if (botonPresionado.equals("bajar")) { //Botón bajar brazo
                    if (mov2 < 1) { //Restricción bajar brazo
                        enviardatos(7); //Enviar dato 7 a Arduino para efectuar la respectiva accion - bajar brazo
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            BajarBrazo(); //Llamar al método
                        }
                        mov2++; //contador restricción
                    }
                    movimientofinalizado = false; //Indicar que el movimiento ha finalizado y se debe detener en java
                }

                //Para ruta 1
                //Movimientos
                // - Bajar brazo
                // - Abrir Gripper
                // - Girar izquierda
                // - Girar muñeca menos
                if (botonPresionado.equals("ruta1")) { //Si el item seleccionado corresponde a la ruta 1

                    if (mov2 < 1) { //Restricción bajar brazo
                        enviardatos(7); // Enviar dato 7 al arduino - corresponde a la función Bajar brazo
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            BajarBrazo(); //Llamar al método
                        }
                        mov2++; //contador restricción
                    } else if (cont < 8) { //Restriccion abrir gripper 
                        enviardatos(1);// Enviar dato 1 al arduino - corresponde a la función Abrir gripper
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            abrirGripper();//Llamar al método
                        }
                        cont++; //contador restricción
                    } else if (mov1 < 1) { //Restricción girar brazo a la izquierda
                        enviardatos(5); // Enviar dato 5 al arduino - corresponde a la función Girar brazo a la izquierda
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            GirarIzquierda();//Llamar al método
                        }
                        mov1++; //contador restricción
                    } else if (cont2 > 0 && cont2 <= 8) { //Restricción girar muñeca menos
                        enviardatos(4); // Enviar dato 4 al arduino - corresponde a la función Girar muñeca menos
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            giramuñecamenos();//Llamar al método
                        }
                        cont2--;//contador restricción
                    }

                    try {
                        Thread.sleep(700); //Hilo duerme 700 ms
                    } catch (InterruptedException ex) {
                        Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                if (botonPresionado.equals("reiniciar")) { //Si el item seleccionado corresponde a la ruta 1
                    if (cont > 0) { //Restricción cerrar gripper
                        enviardatos(2);  // Enviar dato 2 al arduino - corresponde a la función cerrar gripper
                        if (dato == 1) { //Permite cerrar gripper si no ha agarrado un objeto
                            for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                                cerrarGripper();//Llamar al método
                            }
                            cont--;//contador restricción
                        }
                    } else if (cont2 > 4 && cont2 <= 8) { //Restricción girar muñeca menos
                        enviardatos(4); // Enviar dato 4 al arduino - corresponde a la función girar muñeca menos
                        for (int k = 0; k <= 10; k++) {//Animacion en java se repite 10 veces
                            giramuñecamenos(); //Llamar al método
                        }
                        cont2--; //contador restricción
                    } else if (cont2 >= 0 && cont2 < 4) { //Restricción girar muñeca mas
                        enviardatos(3); // Enviar dato 3 al arduino - corresponde a la función girar mueñca mas
                        for (int k = 0; k <= 10; k++) {//Animacion en java se repite 10 veces
                            giramuñecamas(); //Llamar al método
                        }
                        cont2++; //contador restricción
                    } else if (mov1 > 0) { //Restricción girar brazo a la derceha
                        enviardatos(6); // Enviar dato 2 al arduino - corresponde a la función girar brazo a la derecha
                        for (int k = 0; k <= 10; k++) {//Animacion en java se repite 10 veces
                            GirarDerecha(); //Llamar al método
                        }
                        mov1--; //contador restricción
                    } else if (mov2 > 0) { //Restricción subir brazo
                        enviardatos(8);// Enviar dato 8 al arduino - corresponde a la función subir brazo
                        for (int k = 0; k <= 10; k++) {//Animacion en java se repite 10 veces
                            SubirBrazo(); //Llamar al método
                        }
                        mov2--; //contador restricción
                    }

                    try {
                        Thread.sleep(700); //Hilo duerme 700 ms
                    } catch (InterruptedException ex) {
                        Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

                //Para ruta 2
                //Movimientos
                // - Abrir Gripper
                // - Girar muñeca mas
                // - Bajar brazo
                // - Girar a la derecha
                if (botonPresionado.equals("ruta2")) { //Si el item selccionado es ruta 2
                    if (cont < 8) { //Restricción abrir gripper
                        enviardatos(1); // Enviar dato 1 al arduino - corresponde a la función abrir gripper
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            abrirGripper();//Llamar al método
                        }
                        cont++; //contador restricción
                    } else if (cont2 < 8 && cont2 >= 0) { //Restriccion para girar muñeca +
                        enviardatos(3); //Enviar dato 3 a Arduino para efectuar la respectiva accion - girar muñeca +
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            giramuñecamas(); //Llamar al método
                        }
                        cont2++; //Contador restricion
                    } else if (mov2 < 1) { //Restriccion para bajar brazo
                        enviardatos(7); //Enviar dato 7 a Arduino para efectuar la respectiva accion - bajar brazo
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            BajarBrazo(); //Llamar al método
                        }
                        mov2++; //Contador restricion
                    } else if (mov1 > 0) { //Restriccion para girar brazo a la derecha
                        enviardatos(6); //Enviar dato 6 a Arduino para efectuar la respectiva accion girar brazo a la dercecha
                        for (int k = 0; k <= 10; k++) { //Animacion en java se repite 10 veces
                            GirarDerecha();  //Llamar al método
                        }
                        mov1--; //Contador restricion
                    }

                    try {
                        Thread.sleep(700); //Hilo duerme 700 ms
                    } catch (InterruptedException ex) {
                        Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
            try {
                Thread.sleep(200); //Hilo duerme 200 ms
            } catch (InterruptedException ex) {
                Logger.getLogger(BrazoRobotico.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    
    
}
